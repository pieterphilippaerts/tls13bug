﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Authentication;

namespace TLS13Test {
    class Program {
        static void Main(string[] args) {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Running on .NET Core version: " + RuntimeInformation.FrameworkDescription);
            /*
                 Make sure you enable TLS 1.3 support first!
                   => https://stackoverflow.com/questions/56072561/how-to-enable-tls-1-3-in-windows-10
                 
                 If you do not enable TLS 1.3, everything works perfectly; it seems that there's
                 a bug in Windows' TLS 1.3 implementation...
             */

            try {
                DownloadUrl("https://www.googleapis.com/");
            } catch (Exception e) {
                Console.WriteLine("The download of 'https://www.googleapis.com/' failed!!");
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine();

            try {
                DownloadUrl("https://accounts.google.com/");
            } catch (Exception e) {
                Console.WriteLine("The download of 'https://accounts.google.com/' failed!!");
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine();
            Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
        }
        public static void DownloadUrl(string url) {
            try {
                var request = WebRequest.CreateHttp(url);
                request.Method = "GET";
                request.AllowAutoRedirect = true;
                PrintResult(request.GetResponse() as HttpWebResponse);
            } catch (WebException we) {
                var exceptionResponse = we.Response as HttpWebResponse; /* If the server returns an HTTP 4xx or 5xx, HttpWebRequest throws a WebException  */
                if (exceptionResponse != null) {
                    PrintResult(exceptionResponse);
                    return;
                }
                throw; // apparently, the exception was not an HTTP 4xx or 5xx
            }

            void PrintResult(HttpWebResponse response) {
                Console.WriteLine($"The download of '{ response.ResponseUri.ToString() }' succeeded.");
                Console.WriteLine($"The server returned status code { response.StatusCode }");
                var tlsVersion = GetTlsVersion(response.GetResponseStream());
                if (tlsVersion == null) {
                    Console.WriteLine($"The TLS version that was used could not be determined.");
                } else {
                    Console.WriteLine($"The connection used { tlsVersion }");
                    if (tlsVersion != SslProtocols.Tls13) {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("The connection did not use TLS 1.3. Are you sure you correctly enabled TLS 1.3 support in Windows?");
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }
                }
            }
        }
        /// <summary>
        /// This method uses reflection to peek into the internal connection parameters
        /// to extract the TLS version that is used for the connection.
        /// 
        /// This method never throws an exception, but returns null if the TLS version
        /// could not be determined.
        /// </summary>
        private static SslProtocols? GetTlsVersion(Stream s) {
            try {
                var bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
                var streamType = s.GetType();
                var connectionMethod = streamType.GetField("_connection", bindingFlags);
                if (connectionMethod == null)
                    return null;
                var connection = connectionMethod.GetValue(s);
                if (connection == null)
                    return null;
                var connectionType = connection.GetType();
                var streamField = connectionType.GetField("_stream", bindingFlags);
                if (streamField == null)
                    return null;
                var stream = streamField.GetValue(connection);
                var sslStream = stream as SslStream;
                return sslStream?.SslProtocol;
            } catch {
                return null;
            }
        }
    }
}
